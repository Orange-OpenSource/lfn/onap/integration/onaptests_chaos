#!/usr/bin/env python3

#   Copyright 2020 Orange, Ltd.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# Launch basic_* tests in parallel and report results
# the possible basic tests are:
# - basic_onboarding
# - basic_vm
# - basic_network
# - basic_cnf
# - ...

# Dependencies:
#     See requirements.txt
#     The dashboard is based on bulma framework
#
# Environment:
#
# Example usage:
#       python reporter.py
#               -r <reporting path>
#
# the summary html page will be generated where the script is launched
"""Module to launch chaos tests on ONAP."""
from __future__ import print_function
import argparse
import logging
import json
import os
import tempfile

from jinja2 import (  # pylint: disable=import-error
    Environment,
    select_autoescape,
    PackageLoader,
)

# Logger
LOG_LEVEL = 'DEBUG'
logging.basicConfig()
LOGGER = logging.getLogger("onaptests_chaos")
LOGGER.setLevel(LOG_LEVEL)

DIR_NAME = os.path.dirname(__file__)
TMP_DIR = tempfile.gettempdir()
FILENAME = TMP_DIR + "/chaos-results.html"
TEMPLATE_FILE = "chaos-results.html.j2"

PARSER = argparse.ArgumentParser()
PARSER.add_argument(
    '-r', '--reporting',
    help='result file path',
    default=FILENAME)
ARGS = PARSER.parse_args()

def generate_reporting():
    """Generate reporting for the test."""

    # init just connection_check to get the list of scenarios
    # as all the scenarios run connection_check
    # the following tests are the default daily tests
    LOGGER.info("generate Chaos reporting page")
    LOGGER.info("Get the result json")

    json_list = []

    for file in os.listdir(TMP_DIR + "/resiliency"):
        if file.endswith(".json"):
            json_list.append(
                (os.path.join(
                    TMP_DIR + "/resiliency", file)))

    LOGGER.info("List of Jsons : %s", json_list)

    results_dict = []

    for my_json in json_list:

        with open(my_json) as json_file:
            json_object = json.load(json_file)
            json_file.close()

        test_name = json_object['spec']['engine']
        test_result = json_object['status']['experimentStatus']['verdict']
        result_object = {
            "name": test_name,
            "result": test_result,
        }
        if test_result != "Pass":
            fail_step = json_object['status']['experimentStatus']['failStep']
            result_object["fail_step"] = fail_step

        results_dict.append(result_object)

    LOGGER.info("Generate the page ")

    jinja_env = Environment(
        autoescape=select_autoescape(["html"]),
        loader=PackageLoader('onaptests_chaos'))
    jinja_env.get_template(TEMPLATE_FILE).stream(
        data=results_dict).dump(
            "{}".format(ARGS.reporting))
    LOGGER.info("Page generated ")

def main():
    """Entry point"""
    LOGGER.info("****************************")
    LOGGER.info("Generate resiliency reporting")
    LOGGER.info("****************************")
    generate_reporting()

if __name__ == "__main__":
    # execute only if run as a script
    main()
